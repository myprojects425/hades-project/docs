## Boundary objects specification for API

### Zone

```yaml
kind: "Zone",
apiVersion: "v1",
metadata:
   name: "zone.name"
```


### Federation

```yaml
kind: "Federation",
apiVersion: "v1",
metadata:
   name: "federation.name"
zones:
   - "zone.one"
   - "safe.zone"
```

