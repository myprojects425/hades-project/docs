## **zoneclt** Message Broker

This broker is used for intercomunication with components in zone

### Message Broker **channels**
```txt
ZONE_ID
|- SENTINEL_ID
|   |- PACKETS : Intercepted packets by sentinel
|- THREATS : Identified threats in zone
```