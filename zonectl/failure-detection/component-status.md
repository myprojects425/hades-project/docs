## Status of zone components

Depending on the responses to heartbeats from `Failure Detection System` from a component in a zone, it can be in the following states:  

* `UP`
* `DEAD`