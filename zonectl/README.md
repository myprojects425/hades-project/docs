## zonectl

Zone Controller (or `zonectl`) is a group of services to manage components (sentinels, firewalls, ...) in a zone and serves as an entry point to `service provider services` (Event-Broker, Oracles, Report Generator, ...)