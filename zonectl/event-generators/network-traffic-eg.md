## Network Traffic Event Generator

This microservice consume all messages from `ZONE_ID/SENTINEL_ID/PACKETS` and generate suitable batches of events for `event-broker` component (service provider side) 


### Event Description
* Event-Name: `New Intercepted Batch of Network Packets`
* Event-Schema:
```txt
# SCHEMA
```
* a