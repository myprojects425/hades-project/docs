# Configuration Files for `hades-project`

* CONFIGURATION_DIRECTORY: `/etc/hades`
* Directory for optional configuration: `/etc/hades/hades.conf.d`

## Sentinel - Configuration Files
* LOCATION: `/etc/hades/sentinel.conf`

**OPTIONALS**

* PACKET_FILTER_FILE: `/etc/hades/hades.conf.d/00-sentinel-filters.conf`

## ZoneCTL - Configuration Files
* LOCATION: `/etc/hades/zonectl.conf`


### ZoneCTL Components
#### Failure Detection

* LOCATION: `/etc/hades/hades.conf.d/10-zonectl-failure-detection.conf`