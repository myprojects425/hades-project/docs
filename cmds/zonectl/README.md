## zonectl Client

CLI client to manage `zonectl` components

### CLI Structure
```txt
zonectl
|- zone
|   |- create: create zone (register zone object) and generate join mechanism
|   |- list: list registered zones
|   |- components: list registered components to zone
|   |- delete: delete zon (unregister zone object)
|   |- join-token: generate a token to join components to a zone
|- federation
|   |- create: create federation
|   |- delete: delete federation
|   |- join: join to federation
```