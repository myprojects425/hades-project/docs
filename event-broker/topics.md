## Topics for a zone

```txt
ZONE_ID
|- SENTINEL_ID
| |- PACKETS: Intercepted network traffic by sentinel on zone
|- THREATS : Detected threads on zone
|- ALERTS: Alerts to be sent (ephemeral events)
```

**NOTE:** When a threat is detected on a zone that belong to a **federation**, then that *event* is broadcasted to other zones in federation to their *respective topic*.