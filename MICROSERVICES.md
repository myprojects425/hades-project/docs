# Microservices of `hades-project`

General description of functionalities of different microservices of `hades-project` (for more details check components specifications)


## Component types
* `Client Side Components`: components deployed and mantained by users
* `Service Provider Side Components`: components deployed and mantained by service provider


## Client Side Components

### Sentinel
* Intercept network traffic and send them to the controller of its zone (**zonectl**)

More detail about [Sentinel](./client-side/sentinel/)

### zonectl
* Event Generators (from `messages` to `events`)
* Manage operation (create, delete) of zones [1]
* Register zone components (peers) (**LOCAL**)
* Join to federation [1]
* Generate report [1]

[1] forward request to API-Gateway of Service Provider

More detail about [zonectl](./client-side/zonectl/)

### zonectl Components

##### Event Generators
* Transform messages from `message broker channels` and convert them into suitable events for `event-broker` (service provider side)


More detail about [zonectl Event Generators](./client-side/zonectl/message-broker/event-generators/)

#### Failure Detection System
* Monitor state of zone components (peers)
* Detect failure of zone components (peers) using heartbeats 

More detail about [zonectl Failure Detection System](./client-side/zonectl/failure-detection/)

#### Zone Register System
* Register zone components with an specific zone using `Component Register Protocol`

More detail about [zonectl Zone Register System](./client-side/zonectl/register/)

#### Message-Broker
* Provide asynchronous intercomunication between zone components (`sentinel`, `firewall`, `HIDS`) with *zonectl components*

More detail about [zonectl Message-Broker](./client-side/zonectl/message-broker/)

## Service Provider Side Components

### API-Gateway
* Component to proxy API request to `Service Provider Components` 

More detail about [API-Gateway](./service-provider-side/api-gateway/)

### Event-Broker
* Provide asynchronous intercommunication (based on event-driven architecture) between  Service Provider Components


More detail about [Event Broker](./service-provider-side/event-broker/)

### Report Generator

### Alert System

### Register System

### Oracles

More detail about [Oracles](./service-provider-side/oracles/)

#### NIDS Oracles
